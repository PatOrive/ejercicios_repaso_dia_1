"use strict";

/* Ejercicio 1 Leetcode: best-time-to-buy-and-sell-stock

Input: [7, 1, 5, 3, 6, 4]
Output: 5
Explanation: Buy on day 2(price = 1) and sell on day 5
(price = 6), profit = 6 - 1 = 5.
Not 7 - 1 = 6, as selling price needs to be larger 
than buying price.*/

// let prices = [7, 1, 5, 3, 6, 4];

// Solución A:
//
// const maxProfit = function(prices) {
//   let mayorLucro = 0;
//   for (let i = 0; i < prices.length - 1; i++) {
//     for (let j = i + 1; j < prices.length; j++) {
//       //   console.log({ i, j, price_i: prices[i], price_j: prices[j] });
//       const lucro = prices[j] - prices[i];
//       if (lucro > mayorLucro) {
//         mayorLucro = lucro;
//       }
//     }
//   }
//   return mayorLucro;
// };

// Solución B (más correcta):
//
// const maxProfit = function(prices) {
//   let mayorLucro = 0;
//   for (let i = 0; i < prices.length - 1; i++) {
//     for (let j = i + 1; j < prices.length; j++) {
//       //   console.log({ i, j, price_i: prices[i], price_j: prices[j] });
//       const lucro = prices[j] - prices[i];
//       mayorLucro = Math.max(mayorLucro, lucro);
//     }
//   }

//   return mayorLucro;
// };

// console.log(maxProfit(prices));

/* Ejercicio 2 Leetcode: 657. Robot Return to Origin
Example 1:
Input: "UD"
Output: true
Explanation: The robot moves up once, and then down once. 
All moves have the same magnitude, so it ended up at the 
origin where it started. Therefore, we return true.


Example 2:
Input: "LL"
Output: false
Explanation: The robot moves left twice. It ends up two 
"moves" to the left of the origin. We return false because 
it is not at the origin at the end of its moves.

*/

// let input = "UD";

// const judgeCircle = function(moves) {
//   let up = 0;
//   let down = 0;
//   let right = 0;
//   let left = 0;

//   const splitMoves = moves.split("");

//   for (const move of splitMoves) {
//     switch (move) {
//       case "U":
//         up++;
//         break;

//       case "D":
//         down++;
//         break;

//       case "R":
//         right++;
//         break;
//       // el case "R" lo dejamos para el default:
//       default:
//         left++;
//     }
//   }
//   const differenceHorizontalMoves = right - left;
//   const differenceVerticalMoves = up - down;

//   if (differenceHorizontalMoves === 0 && differenceVerticalMoves === 0) {
//     return true;
//   }
//   return false;
// };

// console.log(judgeCircle(input));

/* Ejercicio 3 - parte 1
Valdría tb poner fuera del constructor;
position = 0;*/

// class Robot {
//   constructor(space) {
//     this.space = space;
//     this.position = 0;
//   }
//   moveLeft() {
//     if (this.position === 0) {
//       return false;
//     }

//     this.position--;
//     return true;
//   }
//   moveRight() {
//     if (this.position === this.space.length - 1) {
//       return false;
//     }
//     this.position++;
//     return true;
//   }

//   currentPosition() {
//     return this.space[this.position];
//   }
// }

// const mySpace = [1, 9, 5, 8, 7, 6];
// const myLittlePrettyRobot = new Robot(mySpace);
// console.log(myLittlePrettyRobot);
// console.log(myLittlePrettyRobot.space);
// console.log(myLittlePrettyRobot.space[0]);

// console.log(myLittlePrettyRobot.currentPosition()); // Salida: 1 undefined

// myLittlePrettyRobot.moveRight();
// console.log(myLittlePrettyRobot.moveRight());

// console.log(myLittlePrettyRobot.currentPosition()); // Salida: 9 undefined

// myLittlePrettyRobot.moveLeft();
// console.log(myLittlePrettyRobot.currentPosition()); // Salida: 1 undefined

// myLittlePrettyRobot.moveLeft();
// console.log(myLittlePrettyRobot.currentPosition()); // Salida: 1 undefined

// myLittlePrettyRobot.moveRight();
// myLittlePrettyRobot.moveRight();
// myLittlePrettyRobot.moveRight();
// console.log(myLittlePrettyRobot.currentPosition()); // Salida: 8 undefined

/* Ejercicio 3 - parte 2 : array bidimensional*/

class Robot {
  constructor(space, rowPosition, columnPosition) {
    this.space = space;
    this.rowPosition = 0;
    this.columnPosition = 0;
  }
  moveLeft() {
    if (this.columnnPosition === 0) {
      return false;
    }

    this.columnPosition--;
    return true;
  }
  moveRight() {
    if (this.columnPosition === this.space.length - 1) {
      return false;
    }
    this.columnPosition++;
    return true;
  }

  currentPosition() {
    return this.space[this.rowPosition][this.columnPosition];
  }

  moveUp() {
    if (this.rowPosition === 0) {
      return false;
    }
    this.rowPosition--;
    return true;
  }

  moveDown() {
    if (this.rowPosition === this.space.length - 1) {
      return false;
    }
    this.rowPosition++;
    return true;
  }
}

const mySpace = [
  [1, 9, 5],
  [7, 6, 3],
  [6, 6, 8]
];

const myLittlePrettyRobot = new Robot(mySpace);
// console.log(myLittlePrettyRobot);
// console.log(myLittlePrettyRobot.space);
// console.log(myLittlePrettyRobot.space[1][2]);

console.log(myLittlePrettyRobot.currentPosition()); // Salida: 1

myLittlePrettyRobot.moveRight();
myLittlePrettyRobot.moveRight();
myLittlePrettyRobot.moveRight();
console.log(myLittlePrettyRobot.currentPosition()); // Salida: 5

myLittlePrettyRobot.moveDown();
myLittlePrettyRobot.moveDown();
console.log(myLittlePrettyRobot.currentPosition()); // Salida: 8

myLittlePrettyRobot.moveUp();
console.log(myLittlePrettyRobot.currentPosition()); // Salida: 3
