"use strict";

/* 
EJERCICIO 1 LEET CODE
Array of integers, find if the array contains any duplicates.
Function must return true if any value appears twice or more; false
if every element is distinct.
*/

const nums1 = [1, 2, 3, 1];
const nums2 = [5, 2, 3, 1];

function isRepeat(arrayNums) {
  for (let i = 0; i < arrayNums.length; i++) {
    for (let j = i + 1; j < arrayNums.length; j++) {
      if (arrayNums[i] === arrayNums[j]) {
        return true;
      }
    }
  }
  return false;
}

console.log(isRepeat(nums1));
console.log(isRepeat(nums2));

/*
EJERCICIO 2 LEET CODE
Max consecutive ones. Array binario, encuentra la máxima cantidad de  
1s consecutivos en el array
*/

let arr1 = [1, 1, 0, 1, 1, 1];
let arr2 = [1, 0, 1, 1, 0, 1];

const findMaxConsecutiveOnes = function(nums) {
  let repeatedOnes = 0;
  let mayor = 0;

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] === 1) {
      repeatedOnes++;
      if (repeatedOnes > mayor) {
        mayor = repeatedOnes;
      }
    }
    if (nums[i] === 0) {
      repeatedOnes = 0;
    }
  }
  return mayor;
};

console.log(findMaxConsecutiveOnes(arr1));
console.log(findMaxConsecutiveOnes(arr2));

/*
EJERCICIO 3 LEETCODE
Given an array nums, write a function to move all 0's to the end
of it while maintaining the relative order of the non-zero elements.
Do it without making a copy of the array.
let input = [0, 1, 0, 3, 12];
output = [1, 3, 12, 0, 0];*/

const numbers = [0, 1, 0, 3, 0, 12];

function moveZeroes(nums) {
  for (let i = nums.length - 1; i >= 0; i--) {
    if (nums[i] === 0) {
      nums.splice(i, 1);
      nums.push(0);
    }
  }
  return nums;
}

console.log(moveZeroes(numbers));

//**
//  * ###################################
//  * ###### E J E R C I C I O   1 ######
//  * ###################################
//  *
//  * Sara y Laura juegan al baloncesto en diferentes equipos. En los
//  * últimos 3 partidos, el equipo de Sara anotó 89, 120 y 103 puntos,
//  * mientras que el equipo de Laura anotó 116, 94, y 123 puntos.
//  *
//  * `1.` Calcula la media de puntos para cada equipo.
//  *
//  * `2.` Muestra un mensaje que indique cuál de los dos equipos
//  *      tiene mejor puntuación media. Incluye en este mismo mensaje
//  *      la media de los dos equipos.
//  *
//  * `3.` María también juega en un equipo de baloncesto. Su equipo
//  *      anotó 97, 134 y 105 puntos respectivamente en los últimos
//  *      3 partidos. Repite los pasos 1 y 2 incorporando al equipo
//  *      de María.
//  *
//  */

let saraTeam = [89, 120, 103];
let lauraTeam = [116, 94, 123];

/* Para calcular la media de puntos de cada equipo, creo una variable 
que sume el total de puntos y la divido entre la longitud del array 
(número de partidos) */

function averagePoints(teamPoints) {
  let totalPoints = 0;

  for (let i = 0; i < teamPoints.length; i++) {
    totalPoints += teamPoints[i];
  }
  let averageTeam = totalPoints / teamPoints.length;
  return averageTeam;
}

console.log(averagePoints(saraTeam));
console.log(averagePoints(lauraTeam));

/* Para saber qué equipo tiene mejor puntuación media, creo un "if"
que compare las medias y muestre la frase correspondiente cuando se
cumpla la condición. */

if (averagePoints(saraTeam) > averagePoints(lauraTeam)) {
  console.log(
    `El equipo de Sara tiene mejor puntuación media. La media del equipo de Sara es de ${averagePoints(
      saraTeam
    )} puntos y la media del equipo de Laura es de ${averagePoints(
      lauraTeam
    )} puntos.`
  );
} else if (averagePoints(lauraTeam) > averagePoints(saraTeam)) {
  console.log(
    `El equipo de Laura tiene mejor puntuación media. La media del equipo de Laura es de ${averagePoints(
      lauraTeam
    )} puntos y la media del equipo de Sara es de ${averagePoints(
      saraTeam
    )} puntos.`
  );
} else {
  console.log(`Ambos equipos tienen la misma puntuación media.`);
}

/* Calculo la media del equipo de María con la función definida en el paso 1
y la incorporo al "if" usando el operador AND: &&.*/

let mariaTeam = [97, 134, 105];

console.log(averagePoints(mariaTeam));

if (
  averagePoints(saraTeam) > averagePoints(lauraTeam) &&
  averagePoints(saraTeam) > averagePoints(mariaTeam)
) {
  console.log(
    `El equipo de Sara tiene mejor puntuación media. La media del equipo de Sara es de ${averagePoints(
      saraTeam
    )} puntos, la media del equipo de Laura es de ${averagePoints(
      lauraTeam
    )} puntos y la media del equipo de María es de ${averagePoints(mariaTeam)}.`
  );
} else if (
  averagePoints(lauraTeam) > averagePoints(saraTeam) &&
  averagePoints(lauraTeam) > averagePoints(mariaTeam)
) {
  console.log(
    `El equipo de Laura tiene mejor puntuación media. La media del equipo de Laura es de ${averagePoints(
      lauraTeam
    )} puntos, la media del equipo de Sara es de ${averagePoints(
      saraTeam
    )} puntosy la media del equipo de María es de ${averagePoints(mariaTeam)}.`
  );
} else if (
  averagePoints(mariaTeam) > averagePoints(saraTeam) &&
  averagePoints(mariaTeam) > averagePoints(lauraTeam)
) {
  console.log(
    `El equipo de María tiene mejor puntuación media. La media del equipo de María es de ${averagePoints(
      mariaTeam
    )} puntos, la media del equipo de Sara es de ${averagePoints(
      saraTeam
    )} puntosy la media del equipo de Laura es de ${averagePoints(lauraTeam)}.`
  );
} else {
  console.log(`Todos los equipos tienen la misma puntuación media.`);
}

// /**
//  * ###################################
//  * ###### E J E R C I C I O   2 ######
//  * ###################################
//  *
//  * Jorge y su familia han ido a comer a tres restaurantes distintos.
//  * La factura fue de 124€, 58€ y 268€ respectivamente.
//  *
//  * Para calcular la propina que va a dejar al camarero, Jorge ha
//  * decidido crear un sistema de calculo (una función). Quiere
//  * dejar un 20% de propina si la factura es menor que 50€, un 15%
//  * si la factura está entre 50€ y 200€, y un 10% si la factura es
//  * mayor que 200€.
//  *
//  * Al final, Jorge tendrá dos arrays:
//  *
//  * - `Array 1` Contiene las propinas que ha dejado en cada uno de
//  *    los tres restaurantes.
//  *
//  * - `Array 2` Contiene el total de lo que ha pagado en cada uno de
//  *    los restaurantes (sumando la propina).
//  *
//  * `NOTA` Para calcular el 20% de un valor, simplemente multiplica
//  *  por `0.2`. Este resultado se obtiene de dividir `20/100`. Si
//  *  quisieramos averiguar el 25% de un valor lo multiplicaríamos
//  *  por 0.25.
//  *
//  * `25 / 100 = 0.25`.
//  *
//  */

let invoices = [124, 58, 268];
let tips = [];
let totals = [];

/* Defino array de propinas (tips) como un array vacío. Calculo la propina 
correspondiente a cada rango de facturas usando un "if" y las añado
al array de propinas con un "push". */

function createTips(ammounts) {
  for (let ammount of ammounts) {
    if (ammount < 50) {
      tips.push(Math.round(ammount * 0.2));
    } else if (ammount >= 50 && ammount < 200) {
      tips.push(Math.round(ammount * 0.15));
    } else {
      tips.push(Math.round(ammount * 0.1));
    }
  }
  return tips;
}

console.log(createTips(invoices));

/* Defino array de totales (totals) como un array vacío; este array
contendrá la suma de factura + propina de cada comida. Usando un
"map", indico que a cada importe de factura del arrary "invoice" se 
le sume la propina que tenga la misma posición en el array "tips".*/

const createTotals = invoices.map((invoice, index) => invoice + tips[index]);

console.log(createTotals);

// /**
//  * ###################################
//  * ###### E J E R C I C I O   3 ######
//  * ###################################
//  *
//  * Dado el siguiente array de números:
//  *
//  * `nums = [100, 3, 4, 2, 10, 4, 1, 10]`
//  *
//  * `1.` Recorre todo el array y muestra por consola cada uno de sus
//  *      elementos con la ayuda de un `for`, con la ayuda de un `map`
//  *      y con la ayuda de un `for...of`.
//  *
//  * `2.` Ordena el array de menor a mayor sin emplear `sort()`.
//  *
//  * `3.` Ordena el array de mayor a menor empleando `sort()`.
//  *

let nums = [100, 3, 4, 2, 10, 4, 1, 10];

for (let i = 0; i < nums.length; i++) {
  console.log(nums[i]);
}

let numsMap = nums.map(num => console.log(num));

for (let num of nums) {
  console.log(num);
}

/* Para ordenar el array de menor a mayor, defino un array como copia 
del parámetro -que será un array-. Creo un bucle dentro de otro bucle 
para comparar los valores del array y una variable temporal para 
guardar los valores que voy cambiando. Cada vez que un valor 
bucle "i" sea mayor que un valor del bucle "j",se intercambian las
posiciones en el array, de forma que el mayor valor se vaya a la derecha.*/

function orderArr(desordered) {
  const ordered = [...desordered];
  for (let i = 0; i < ordered.length; i++) {
    for (let j = 0; j < ordered.length; j++) {
      if (ordered[i] < ordered[j]) {
        const temp = ordered[j];
        ordered[j] = ordered[i];
        ordered[i] = temp;
      }
    }
  }
  return ordered;
}

const ordered = orderArr(nums);
console.log(ordered);

/* El método "sort" convierte los elementos de un array a strings. Para 
ordenar números con "sort" hay que usar la función "compare" con dos
parámetros (elementos a comparar). La función define el orden de salida; 
como queremos que sea orden descendente, indicamos que si el 2º elemento
es mayor que el primero, el segundo elemento irá en una posición anterior
al primero. Si el 2º elemento es menor, el 1º irá antes; y si son iguales
no se mueven.
  */

const ordered2 = nums.sort(compare);

function compare(num1, num2) {
  return num2 - num1;
}

console.log(ordered2);

// /**
//  * ###################################
//  * ###### E J E R C I C I O   4 ######
//  * ###################################
//  *
//  * Crea una `arrow function` que reciba dos números por medio del
//  * `prompt`, reste ambos números, y nos devuelva el resultado.
//  * En caso de que el resultado sea negativo debe cambiarse a
//  * positivo. Este resultado se mostrará por medio de un `alert`.
//  *
//  */

/* Creo la arrow function e incluyo un if para indicar qué hacer en 
caso de que se cumpla la condición establecida en el enunciado, de 
cambiar el signo de la resta si da un valor negativo. */

let numero1 = parseInt(prompt("Dime un número"));
let numero2 = parseInt(prompt("Dime otro número"));
let resultado;

let resta = (numero1, numero2) => {
  if (numero1 < numero2) {
    return (resultado = -(numero1 - numero2));
  } else {
    return (resultado = numero1 - numero2);
  }
};
alert(resta(numero1, numero2));
